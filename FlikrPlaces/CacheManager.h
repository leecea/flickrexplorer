//
//  cacheManager.h
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 10/26/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CacheManager : NSObject

+ (void)cachePhoto:(NSDictionary *)photo withImage:(UIImage *)image;
+ (NSURL *)getCachedURL:(NSDictionary *)photo;
+ (BOOL)createCacheDir;

@end

//
//  cacheManager.m
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 10/26/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import "CacheManager.h"
#import "ImageData.h"
#import "Foundation/NSFileManager.h"

@implementation CacheManager

#define CACHE_LIMIT     (10.0*1024*1024)
#define CACHE_NAME      @"FlickrCache"

+ (NSURL *)cacheURL
{
    NSURL *cacheLoc = [[[NSFileManager defaultManager] URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask] lastObject];
    return [cacheLoc URLByAppendingPathComponent:CACHE_NAME];
}

+ (void)cachePhoto:(NSDictionary *)photo withImage:(UIImage *)image
{
    if(![self getCachedURL:photo]){
        
        NSData *imageData = UIImageJPEGRepresentation(image, 1.0);      // change image to data
        
        [self makeSpaceForImage:imageData];                             // delete some old cached files if cache space is full
    
        NSString *fileName = [ImageData imageIdentifier:photo];
        NSURL *fileURL = [[NSURL alloc] initWithString:fileName relativeToURL:[self cacheURL]];
    
        [imageData writeToURL:fileURL atomically:YES];
    } 
}

// call this in app delegate then no need to keep checking
//
+ (BOOL)createCacheDir
{
    BOOL isDir;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *cacheLoc = [self cacheURL];
    
    if([fileManager fileExistsAtPath:[cacheLoc path] isDirectory:&isDir] && isDir) return YES;  // already exists
    
    // This is our sandbox, so nothing else should be there, but just in case...
    [fileManager removeItemAtURL:cacheLoc error:nil];
    
    return [fileManager createDirectoryAtURL:cacheLoc withIntermediateDirectories:YES attributes:nil error:nil];
}

+ (void)makeSpaceForImage:(NSData *)imageData
{
    CGFloat imageSize = imageData.length;
    CGFloat cachedSpace = 0.0;
    NSURL *fileURL;
    
    NSArray *allURLs = [self getAllCachedURLs];
    
    for (fileURL in allURLs) {
        cachedSpace += [self getFileSize:fileURL];
    }

    if((cachedSpace + imageSize) > CACHE_LIMIT){
        
        // need to remove some older files so sort by created date

        allURLs = [allURLs sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            
            NSDate *createDate1;
            [obj1 getResourceValue:&createDate1 forKey:NSURLCreationDateKey error:nil];
            NSDate *createDate2;
            [obj2 getResourceValue:&createDate2 forKey:NSURLCreationDateKey error:nil];
            
            return [createDate1 compare:createDate2];
        }];
        
        // delete files at front of sorted array until there is enough space
        //
        NSFileManager *fileManager = [NSFileManager defaultManager];
        for(fileURL in allURLs){

            cachedSpace -= [self getFileSize:fileURL];                  // how big is the file?
            [fileManager removeItemAtURL:fileURL error:nil];            // delete it
            if((cachedSpace + imageSize) <= CACHE_LIMIT) break;         // break if we have enough space now
        }
    }
}

+ (CGFloat)getFileSize:(NSURL *)fileOnDisk
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    return [[[fileManager attributesOfItemAtPath:[fileOnDisk path] error:nil] valueForKey:NSFileSize] floatValue];
}
       
+ (NSURL *)getCachedURL:(NSDictionary *)photo
{
    NSString *fileName = [ImageData imageIdentifier:photo];
    NSArray *fileURLs = [self getAllCachedURLs];
    
    for(NSInteger ix = 0; ix < [fileURLs count]; ix++)
    {
        if([fileName isEqualToString:[fileURLs[ix] lastPathComponent]]) return fileURLs[ix];
    }
    return nil;
}

+ (NSArray *)getAllCachedURLs
{
    NSArray *attributes = [[NSArray alloc] initWithObjects:NSURLIsDirectoryKey,NSURLCreationDateKey,NSURLContentAccessDateKey,NSURLNameKey,nil];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    return [fileManager contentsOfDirectoryAtURL:[self cacheURL] includingPropertiesForKeys:attributes options:NSDirectoryEnumerationSkipsHiddenFiles error:nil];
}

@end

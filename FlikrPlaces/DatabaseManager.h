//
//  VacationManager.h
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 11/6/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Photo+Lifecycle.h"

@interface DatabaseManager : NSObject

+ (void)openDatabase;

+ (BOOL)isSaved:(NSDictionary *)photoDict;
+ (void)save:(NSDictionary *)photoDict;
+ (BOOL)removePhoto:(Photo *)photo;
+ (NSFetchedResultsController *)fetchAllPhotosRC;

@end

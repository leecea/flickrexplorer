//
//  VacationManager.m
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 11/6/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import "DatabaseManager.h"
#import "Photo+Lifecycle.h"
#import "Place.h"
#import "Tag.h"
#import "FlickrFetcher.h"
#import "PlacesAppDelegate.h"

// Database directory and file names
//
#define SAVE_DB_DIR      @"SavedPhotosDir"
#define SAVE_DB_NAME     @"SavedPhotosDB"

// Photo related strings used in queries
//
#define ENTITY       @"Photo"
#define SORT_KEY     @"saveDate"
#define FLICKR_ID    @"flikrId"

@implementation DatabaseManager

// Called in App Delegate to create MOC
//
+ (void)openDatabase
{
    PlacesAppDelegate *appDelegate = (PlacesAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSURL *databaseURL = [[self databaseDirURL] URLByAppendingPathComponent:SAVE_DB_NAME];
    
    // Create the document and open if a match exists on file.
    //
    UIManagedDocument *databaseDocument = [[UIManagedDocument alloc] initWithFileURL:databaseURL];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[databaseURL path]]) {
        
        [databaseDocument openWithCompletionHandler:^(BOOL success) {
            if(success) appDelegate.context = databaseDocument.managedObjectContext;
            else NSLog(@"Database open failed: %@",databaseURL);
        }];
    }
    else {
        
        [databaseDocument saveToURL:databaseURL forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success) {
            if(success) appDelegate.context = databaseDocument.managedObjectContext;
            else NSLog(@"Database create failed: %@",databaseURL);
        }];
    }
}

// Get the MOC from the App Delegate
//
+ (NSManagedObjectContext *)getContext
{
    PlacesAppDelegate *appDelegate = (PlacesAppDelegate *)[[UIApplication sharedApplication] delegate];
    return appDelegate.context; 
}

+ (NSURL *)databaseDirURL
{
    BOOL isDir;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // construct the save directory URL
    //
    NSURL *dirURL = [[fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    dirURL = [dirURL URLByAppendingPathComponent:SAVE_DB_DIR];
    
    // check that it exists and is a directory, otherwise create it
    //
    if( ! ([fileManager fileExistsAtPath:[dirURL path] isDirectory:&isDir] && isDir)) {
        [fileManager removeItemAtURL:dirURL error:nil];   // in case a plain file is blocking us
        [fileManager createDirectoryAtURL:dirURL withIntermediateDirectories:YES attributes:nil error:nil];
    }
        
    return dirURL;
}

// Query the database to see if a photo is already saved
//
+ (BOOL)isSaved:(NSDictionary *)photoDict
{
    NSError *error;
    Photo *photo = nil;
    NSManagedObjectContext *context = [self getContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:ENTITY inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K MATCHES %@",FLICKR_ID,[photoDict objectForKey:FLICKR_PHOTO_ID]];

    [fetchRequest setPredicate:predicate];
    
    photo = [[context executeFetchRequest:fetchRequest error:&error] lastObject];
    
    //NSArray *photoArray;
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K MATCHES %@",FLICKR_ID,@"11986834084"];
    //photoArray = [context executeFetchRequest:fetchRequest error:&error];//lastObject];
    //for(photo in photoArray) NSLog(@"Photo Id = %@",photo.flikrId);
    
    if(error) NSLog(@"Error in isSaved");
    
    return (photo != nil);
}

// Save photo (ignore request if it has already been saved)
//
+ (void)save:(NSDictionary *)photoDict
{
    NSError *error;
    
    if(![self isSaved:photoDict]){
        
        NSManagedObjectContext *context = [self getContext];
        
        Photo *photo = [NSEntityDescription insertNewObjectForEntityForName:@"Photo" inManagedObjectContext:context];
        [photo initWithDictionary:photoDict inContext:context];
        
        if(![context save:&error]) NSLog(@"Database Save Error: %@",error);
    }
}

+ (BOOL)removePhoto:(Photo *)photo
{
    NSError *error;
    NSManagedObjectContext *context = [self getContext];
    
    [context deleteObject:photo];
    
    return [context save:&error];
}

+ (NSFetchedResultsController *)fetchAllPhotosRC;
{
    NSFetchedResultsController *controller;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:ENTITY inManagedObjectContext:[self getContext]];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:SORT_KEY ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    
    [fetchRequest setFetchBatchSize:20];
    
    controller = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:[self getContext] sectionNameKeyPath:nil
                                                   cacheName:@"FlickrRoot"];
    return controller;
}

+ (NSFetchedResultsController *)fetchPhotoWithFlikrIdRC:(NSString *)flikrId;
{
    NSFetchedResultsController *controller;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:ENTITY inManagedObjectContext:[self getContext]];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%@ = %@",FLICKR_ID,flikrId];
    [fetchRequest setPredicate:predicate];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:SORT_KEY ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    
    [fetchRequest setFetchBatchSize:20];
    
    controller = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                     managedObjectContext:[self getContext] sectionNameKeyPath:nil
                                                                cacheName:@"FlickrRoot2"];
    return controller;
}


@end

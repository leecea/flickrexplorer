//
//  Defaults.h
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 9/24/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#ifndef FlikrPlaces_Defaults_h
#define FlikrPlaces_Defaults_h

#define EMPTY_STRING @""
#define RECENT_PHOTO_KEY    @"FlickrPlace.Recent"
#define MAX_RECENT          25

#endif

//
//  ImageData.h
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 9/22/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageData : NSObject

@property (nonatomic, strong) NSURL *imageURL;

+ (UIImage *)getThumbnail:(NSDictionary *)photo;
+ (NSString *)imageIdentifier:(NSDictionary *)photo;

- (UIImage *)getImage:(NSDictionary *)photo;

@end

//
//  ImageData.m
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 9/22/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import "ImageData.h"
#import "FlickrFetcher.h"
#import "CacheManager.h"

@implementation ImageData

+ (UIImage *)getThumbnail:(NSDictionary *)photo
{
    NSURL *imageURL = [FlickrFetcher urlForPhoto:photo format:FlickrPhotoFormatSquare];
    
    UIImage *pImage=[UIImage imageWithData:[NSData dataWithContentsOfURL:imageURL]];
    
    return pImage;
}

+ (NSString *)imageIdentifier:(NSDictionary *)photo
{
    return [photo valueForKey:FLICKR_PHOTO_ID];
}

- (UIImage *)getImage:(NSDictionary *)photo
{
    NSURL *imageURL = [CacheManager getCachedURL:photo];       
    
    if(!imageURL) imageURL = [FlickrFetcher urlForPhoto:photo format:FlickrPhotoFormatLarge];        
             
    return[UIImage imageWithData:[NSData dataWithContentsOfURL:imageURL]];                          
}

@end

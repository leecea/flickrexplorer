//
//  ImageInfoController.h
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 9/27/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "ImageViewController.h"

@interface ImageInfoController : UIViewController

@property (nonatomic, strong) NSDictionary *photo;
@property (weak, nonatomic) IBOutlet MKMapView *infoMap;
@property (strong, nonatomic) ImageViewController *delegate;

@end

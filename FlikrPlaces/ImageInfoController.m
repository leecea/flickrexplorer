//
//  ImageInfoController.m
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 9/27/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import "ImageInfoController.h"
#import "FlickrFetcher.h"
#import "Defaults.h"

@interface ImageInfoController ()

@property (weak, nonatomic) IBOutlet UIView *imageInfoView;
@property (weak, nonatomic) IBOutlet UITextView *imageInfoText;

@end

@implementation ImageInfoController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Set the descriptive text
    
    NSString *titleText = [self.photo valueForKey:FLICKR_PHOTO_TITLE];
    NSString *descText = [self stringByDecodingXMLEntities:[self.photo valueForKeyPath:FLICKR_PHOTO_DESCRIPTION]];
    
    if([titleText isEqualToString:EMPTY_STRING] && [descText isEqualToString:EMPTY_STRING]){
        titleText = @"No description available";
    } else {
        titleText = [titleText stringByAppendingString:[@"\n\n" stringByAppendingString:descText]];
    }
    self.imageInfoText.text = titleText;
    
    // Set the map
    
    // Show a 5 km region around the photo location
    self.infoMap.region = MKCoordinateRegionMakeWithDistance(self.photoCoordinates, 5000, 5000);
    
    // Add a pin at the photo location
    MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
    annotationPoint.coordinate = self.photoCoordinates;
    [self.infoMap addAnnotation:annotationPoint];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CLLocationCoordinate2D)photoCoordinates
{
    CLLocationCoordinate2D point;
    point.latitude = [[self.photo valueForKey:FLICKR_LATITUDE] doubleValue];
    point.longitude = [[self.photo valueForKey:FLICKR_LONGITUDE] doubleValue];
    
    return point;
}

// parsing routine taken from Internet and modified for my project
//
- (NSString *)stringByDecodingXMLEntities:(NSString *)inputString {
    NSUInteger myLength = [inputString length];
    NSUInteger ampIndex = [inputString rangeOfString:@"&" options:NSLiteralSearch].location;
    
    // Short-circuit if there are no ampersands.
    if (ampIndex == NSNotFound) {
        return inputString;
    }
    // Make result string with some extra capacity.
    NSMutableString *result = [NSMutableString stringWithCapacity:(myLength * 1.25)];
    
    // First iteration doesn't need to scan to & since we did that already, but for code simplicity's sake we'll do it again with the scanner.
    NSScanner *scanner = [NSScanner scannerWithString:inputString];
    
    [scanner setCharactersToBeSkipped:nil];
    
    NSCharacterSet *boundaryCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@" \t\n\r;"];
    
    do {
        // Scan up to the next entity or the end of the string.
        NSString *nonEntityString;
        if ([scanner scanUpToString:@"&" intoString:&nonEntityString]) {
            [result appendString:nonEntityString];
        }
        if ([scanner isAtEnd]) {
            goto finish;
        }
        // Scan either a HTML or numeric character entity reference.
        if ([scanner scanString:@"&amp;" intoString:NULL])
            [result appendString:@"&"];
        else if ([scanner scanString:@"&apos;" intoString:NULL])
            [result appendString:@"'"];
        else if ([scanner scanString:@"&quot;" intoString:NULL])
            [result appendString:@"\""];
        else if ([scanner scanString:@"&lt;" intoString:NULL])
            [result appendString:@"<"];
        else if ([scanner scanString:@"&gt;" intoString:NULL])
            [result appendString:@">"];
        else if ([scanner scanString:@"&#" intoString:NULL]) {
            BOOL gotNumber;
            unsigned charCode;
            NSString *xForHex = @"";
            
            // Is it hex or decimal?
            if ([scanner scanString:@"x" intoString:&xForHex]) {
                gotNumber = [scanner scanHexInt:&charCode];
            }
            else {
                gotNumber = [scanner scanInt:(int*)&charCode];
            }
            
            if (gotNumber) {
                [result appendFormat:@"%C", (unichar)charCode];
                
                [scanner scanString:@";" intoString:NULL];
            }
            else {
                NSString *unknownEntity = @"";
                
                [scanner scanUpToCharactersFromSet:boundaryCharacterSet intoString:&unknownEntity];
                
                [result appendFormat:@"&#%@%@", xForHex, unknownEntity];
            }
        }
        else {
            NSString *amp;
            
            [scanner scanString:@"&" intoString:&amp];  //an isolated & symbol
            [result appendString:amp];
        }
        
    }
    while (![scanner isAtEnd]);
    
finish:
    return result;
}
@end

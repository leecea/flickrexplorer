//
//  ImageViewController.h
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 9/19/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import <UIKit/UIKit.h>

#define VISIT_TITLE         @"Save"
#define UNVISIT_TITLE       @""

@interface ImageViewController : UIViewController

@property (nonatomic, strong) NSDictionary *photo;

- (void)displayPhoto;

@end

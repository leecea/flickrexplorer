//
//  ImageViewController.m
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 9/19/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import "ImageViewController.h"
#import "ImageInfoController.h"
#import "ImageData.h"
#import "FlickrFetcher.h"
#import "Defaults.h"
#import "CacheManager.h"
#import "DatabaseManager.h"
#import "UIKit/UIPopoverController.h"

// Action Sheet button titles
//
#define SAVE_TO_DEVICE      @"Save to Photo Library"
#define SAVE_TO_APP         @"Save to Favorites"

@interface ImageViewController () <UIScrollViewDelegate, UISplitViewControllerDelegate, UIToolbarDelegate, UIActionSheetDelegate>

@property (nonatomic, strong) UIImage *imageToDisplay;
@property (nonatomic, strong) ImageData *imageData;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *infoButton;
@property (nonatomic, strong) UIActivityIndicatorView *spinner;
@property (nonatomic, weak) ImageInfoController *infoVC;
@property (nonatomic, weak) UIPopoverController *popover;

@end

@implementation ImageViewController

- (ImageData *)imageData {
    if(_imageData == nil) _imageData = [[ImageData alloc] init];
    return _imageData;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

// Actions linked to bar buttons
//
- (IBAction)infoPressed:(UIBarButtonItem *)sender
{
    if(self.popover){
        [self.popover dismissPopoverAnimated:NO];                           // if there is already a popover, dismiss it
    } else {
        [self performSegueWithIdentifier:@"InfoSegue" sender:self];         // otherwise, segue as normal
    }
}

- (IBAction)savePressed:(UIBarButtonItem *)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:SAVE_TO_APP, SAVE_TO_DEVICE, nil];

    [actionSheet showFromBarButtonItem:sender animated:YES];
}

// Are we in a split view
//
- (BOOL)inSplitView
{
    return (self.splitViewController != nil);
}

// Called by toolBar to set position in iOS7 and give proper UI layout
//
- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar
{
    return UIBarPositionTopAttached;
}

// Update the list of recently viewed photos - if this photo is not in the list, add it to the list and bump out
// the oldest one if needed
//
- (void)updateMostRecent:(NSDictionary *)photo
{
    NSString *currentPhotoId = (NSString *)[photo objectForKey:FLICKR_PHOTO_ID];   // id for currently viewed photo

    // get array of recent images from defaults
    //
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *recentPhotos = [[defaults arrayForKey:RECENT_PHOTO_KEY] mutableCopy];
    
    if(!recentPhotos) recentPhotos = [[NSMutableArray alloc] initWithCapacity:MAX_RECENT];  // doesn't exist, so initialize
    
    // look this image up in the list of recent images
    //
    NSUInteger foundIx = [recentPhotos indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        
        NSString *thisPhotoId = (NSString *)[obj objectForKey:FLICKR_PHOTO_ID];
        
        if([thisPhotoId isEqualToString:currentPhotoId]){
            *stop = YES;
            return TRUE;
        }
        else return FALSE;
    }];
    
    // if current image was not found in list of recent images, add it
    //
    if(foundIx == NSNotFound){
        
        if([recentPhotos count] == MAX_RECENT) [recentPhotos removeLastObject];   // bump off the last one if needed

        [recentPhotos insertObject:self.photo atIndex:0];
        
        [defaults setObject:recentPhotos forKey:RECENT_PHOTO_KEY];
        [defaults synchronize];
    }
}

- (void)displayPhoto
{
    [self updateMostRecent:self.photo];     // update list of recent photos
    
    self.scrollView.delegate = self;
    self.scrollView.zoomScale = 1.0;
    
    [self.spinner startAnimating];
    
    dispatch_queue_t downloadQueue = dispatch_queue_create("FlikrPlaces.Background", NULL);
    dispatch_async(downloadQueue, ^{
        
        self.imageToDisplay = [self.imageData getImage:self.photo];                                 
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.spinner stopAnimating];
            
            self.imageView.image = self.imageToDisplay;
            self.imageView.frame = CGRectMake(0.0, 0.0, self.imageToDisplay.size.width, self.imageToDisplay.size.height);
            
            self.scrollView.contentSize = self.imageToDisplay.size;
            self.scrollView.maximumZoomScale = 2.0;
            
            self.scrollView.showsHorizontalScrollIndicator = NO;
            self.scrollView.showsVerticalScrollIndicator = NO;
            
            // calculate height and width ratios between image and view size
            CGFloat hRatio = self.view.frame.size.height/self.imageToDisplay.size.height;
            CGFloat wRatio = self.view.frame.size.width/self.imageToDisplay.size.width;
            
            self.scrollView.minimumZoomScale = MIN(hRatio, wRatio);
            self.scrollView.zoomScale = MAX(hRatio, wRatio);            // set zoom so image to fills screen
            
            // if we have set the zoom scale based on height, see if we need to center the image horizontally
            if(hRatio > wRatio){
                
                // calculate the offset required to center the image horizontally
                //
                CGFloat hOffset = (self.imageToDisplay.size.width * hRatio - self.view.frame.size.width)/2;
                
                if(hOffset > 0) self.scrollView.contentOffset = CGPointMake(hOffset, 0);
            }
        });
        
        [CacheManager cachePhoto:self.photo withImage:self.imageToDisplay];
    });
}

#pragma mark -
#pragma mark Action Sheet Delegate protocol

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *buttonClicked = [actionSheet buttonTitleAtIndex:buttonIndex];
    
    if([buttonClicked isEqualToString:SAVE_TO_APP]){
        
        // save current image metadata to local database
        [DatabaseManager save:self.photo];
        
    } else if([buttonClicked isEqualToString:SAVE_TO_DEVICE]){
        
        // save current image to device photo library
        UIImageWriteToSavedPhotosAlbum(self.imageToDisplay, nil, nil, nil);
    }
}

#pragma mark -
#pragma mark View load and appear

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.toolBar.delegate = self;
    self.splitViewController.delegate = self;
    
    // add activity indicator view
    //
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.spinner.center = self.view.center;
    [self.imageView addSubview:self.spinner];
    
    /* This needs to be moved and changed since we've added an ActionSheet
    ----------------------------------------------------------------------
    // manage the buttons differently for split view and regular view
    //
    if([self inSplitView]){
        
        // if the photo is already saved, disable the save button
        //
        if([DatabaseManager isSaved:self.photo]){
            self.saveButton.enabled = NO;
        }
    }
    else {
    
        // create right bar button items
        //
        UIBarButtonItem *infoButton = [[UIBarButtonItem alloc] initWithTitle:@"Info" style:UIBarButtonItemStyleBordered target:self action:@selector(infoPressed:)];
        
        if(![DatabaseManager isSaved:self.photo]){
            
            UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(savePressed:)];
            self.navigationItem.rightBarButtonItems = @[infoButton, saveButton];
        }
        else {
            self.navigationItem.rightBarButtonItems = @[infoButton];
        }
    }
    */
    
    // In a split view, right bar buttons are created in the storyboard using a toolbar.
    // Outside a split view, it's a navigation bar/item, so must create them in code
    //
    if(![self inSplitView]){
        UIBarButtonItem *infoButton = [[UIBarButtonItem alloc] initWithTitle:@"Info" style:UIBarButtonItemStyleBordered target:self action:@selector(infoPressed:)];
        UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(savePressed:)];
        self.navigationItem.rightBarButtonItems = @[infoButton, saveButton];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    // If we are returning to this view from another page we will already have an image and we want to
    // preserve the zoom and scroll settings the user has, so don't re-do the logic below.  Only do it when
    // there is no image.
    // 
    if(self.photo && !self.imageToDisplay){
        
        [self displayPhoto];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"InfoSegue"]){
        
        self.infoVC = segue.destinationViewController;
        self.infoVC.photo = self.photo;
        self.infoVC.delegate = self;
        
        // if we are in a split view, remember the popover controller so we can dismiss it later
        //
        if([self inSplitView]) {
            self.popover = [(UIStoryboardPopoverSegue *)segue popoverController];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark UIScrollView Delegate protocol

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

#pragma mark -
#pragma mark Implement split view delegate calls

- (void)splitViewController:(UISplitViewController *)svc willHideViewController:(UIViewController *)aViewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)pc
{
    // called when hiding master, so display the button
    //
    barButtonItem.title = @"Lists";
    [self setSplitViewBarButtonItem:barButtonItem];
}

- (void)splitViewController:(UISplitViewController *)svc willShowViewController:(UIViewController *)aViewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // called when showing master, so hide button
    //
    [self setSplitViewBarButtonItem:nil];
}

- (void)setSplitViewBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    NSMutableArray *toolbarItems = [self.toolBar.items mutableCopy];
    if (barButtonItem) {
        [toolbarItems insertObject:barButtonItem atIndex:0];
    } else {
        [toolbarItems removeObjectAtIndex:0];
    }
    self.toolBar.items = toolbarItems;
}

@end

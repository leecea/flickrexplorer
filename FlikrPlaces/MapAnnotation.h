//
//  MapAnnotation.h
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 10/23/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MapAnnotation : NSObject <MKAnnotation>

@property (nonatomic, weak) NSString *title;
@property (nonatomic, weak) NSString *subtitle;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, assign) NSInteger dictIndex;

- (id)initWithTitle:(NSString *)title subTitle:(NSString *)subTitle dictIndex:(NSInteger)dictIndex coordinate:(CLLocationCoordinate2D)coordinate;

- (NSInteger)getDictIndex;

@end

//
//  MapAnnotation.m
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 10/23/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import "MapAnnotation.h"

@implementation MapAnnotation 

- (id)initWithTitle:(NSString *)title subTitle:(NSString *)subTitle dictIndex:(NSInteger)dictIndex coordinate:(CLLocationCoordinate2D)coordinate
{
    if((self = [super init])){
        self.title = title;
        self.subtitle = subTitle;
        self.coordinate = coordinate;
        self.dictIndex = dictIndex;
    }
    return self;
}

- (NSInteger)getDictIndex
{
    return self.dictIndex;
}

@end

//
//  MapViewController.h
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 10/22/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MapViewController : UIViewController <MKMapViewDelegate>

@property (nonatomic, strong) NSArray *locations;
@property (nonatomic, assign) BOOL locationIsPhoto;

@end

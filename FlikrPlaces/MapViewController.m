//
//  MapViewController.m
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 10/22/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import "MapViewController.h"
#import "MapAnnotation.h"
#import "FlickrFetcher.h"
#import "PhotosViewController.h"
#import "ImageViewController.h"
#import "ImageData.h"
#import "Defaults.h"

typedef struct {
    CLLocationCoordinate2D topLeft;
    CLLocationCoordinate2D bottomRight;
} MapBoundary;

MapBoundary boundary;

@interface MapViewController ()

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) NSDictionary *objectSentAcrossSegue;
@property (nonatomic, assign) BOOL viewJustLoaded;

@end

@implementation MapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)initBoundary
{
    boundary.topLeft.latitude = -90.0;
    boundary.topLeft.longitude = 180.0;
    boundary.bottomRight.latitude = 90.0;
    boundary.bottomRight.longitude = -180.0;
}

- (void)updateBoundary: (CLLocationCoordinate2D) point
{
    if(point.latitude > boundary.topLeft.latitude) boundary.topLeft.latitude = point.latitude;
    if(point.latitude < boundary.bottomRight.latitude) boundary.bottomRight.latitude = point.latitude;
    
    if(point.longitude < boundary.topLeft.longitude) boundary.topLeft.longitude = point.longitude;
    if(point.longitude > boundary.bottomRight.longitude) boundary.bottomRight.longitude = point.longitude;
}

#define PHOTO_TITLE_MAX 20
#define MAP_MARGIN      1.1     // increase map span to make sure the outside points have some space

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.viewJustLoaded = YES;
    
    self.mapView.delegate = self;   
    
    // Loop through the locations to find the enclosing map boundary and add the annotations
    //
    CLLocationCoordinate2D point;
    NSDictionary *row;
    
    [self initBoundary];

    for (NSInteger ix = 0; ix < [self.locations count]; ix++){
        
        row = self.locations[ix];
        
        point.latitude = [[row valueForKey:FLICKR_LATITUDE] doubleValue];
        point.longitude = [[row valueForKey:FLICKR_LONGITUDE] doubleValue];
        [self updateBoundary:point];
        
        NSString *title;
        NSString *subTitle;
        
        if(self.locationIsPhoto){
            title = [row valueForKey:FLICKR_PHOTO_TITLE];
            subTitle = [row valueForKeyPath:FLICKR_PHOTO_DESCRIPTION];
        } else {
            title = [row valueForKey:FLICKR_PLACE_NAME];
            subTitle = nil;
        }
        if([title isEqualToString:EMPTY_STRING]) title = @"Unknown";     
        
        // Send across the index (as dictIndex) into the locations array so it can be used to identify the location for each
        // annotation, when the callout's accessory button is selected
        //
        MapAnnotation *annotation = [[MapAnnotation alloc] initWithTitle:title subTitle:subTitle dictIndex:ix coordinate:point];
        [self.mapView addAnnotation:annotation];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    // Only want this to run when view is first loaded BUT map dimensions are not accurate until now, so cannot run in viewDidLoad.
    // Solution is to use flag.
    //
    if(self.viewJustLoaded){
        
        self.viewJustLoaded = NO;
        
        // create region for map display
        //
        MKCoordinateRegion region;
        
        if([self.locations count] == 0){
            
            region.center.latitude = 0.0;               // nothing to display so set default region to stop map crashing
            region.center.longitude = 0.0;
            region.span.latitudeDelta = 50.0;
            region.span.longitudeDelta = 50.0;
        }
        else {
            region.center.latitude = (boundary.topLeft.latitude + boundary.bottomRight.latitude)/2.0;
            region.center.longitude = (boundary.bottomRight.longitude + boundary.topLeft.longitude)/2.0;
            
            region.span.latitudeDelta = (boundary.topLeft.latitude - boundary.bottomRight.latitude)*MAP_MARGIN;
            region.span.longitudeDelta = (boundary.bottomRight.longitude - boundary.topLeft.longitude)*MAP_MARGIN;
        }
        
        [self.mapView setRegion:region];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"PhotoListSegue"]){

        // Set the selected place in the Photo VC
        //
        PhotosViewController *photosVC = segue.destinationViewController;
        photosVC.place = self.objectSentAcrossSegue;
    }
    
    if([segue.identifier isEqualToString:@"ImageSegue"]){
        
        // Set the selected place in the Photo VC
        //
        ImageViewController *imageVC = segue.destinationViewController;
        imageVC.photo = self.objectSentAcrossSegue;
    }
}

#pragma mark - Map View Delegate protocol

#define PINVIEW         @"PinView"
#define THUMBNAIL_SIZE  35.0

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    MKPinAnnotationView *pin = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:PINVIEW];
    
    if(!pin){
        pin = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:PINVIEW];
        pin.canShowCallout = YES;
        
        // add disclosure button
        //
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        pin.rightCalloutAccessoryView = rightButton;
        
        // add thumbnail view for photos, but don't get the image until the annotation is selected
        //
        if(self.locationIsPhoto){
            UIImageView *thumbnail = [[UIImageView alloc] initWithFrame:CGRectMake(0.0,0.0,THUMBNAIL_SIZE,THUMBNAIL_SIZE)];
            pin.leftCalloutAccessoryView = thumbnail;
        }
    }
    else pin.annotation = annotation;
    
    return pin;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if(self.locationIsPhoto){
        
        // get image for left thumbnail view
        //
        UIImageView *thumbnail = (UIImageView *)view.leftCalloutAccessoryView;
        
        dispatch_queue_t downloadQueue = dispatch_queue_create("FlikrPlaces.Background", NULL);
        dispatch_async(downloadQueue, ^{
         
            UIImage *thumbnailImage = [[ImageData class] getThumbnail:self.locations[[(MapAnnotation *)view.annotation getDictIndex]]];
         
            dispatch_async(dispatch_get_main_queue(), ^{
                thumbnail.image = thumbnailImage;
            });
        });
    }
}

// check to see whether the mapViewController is inside a split view controller
//
- (ImageViewController *) splitViewImageViewController
{
    // get last view controller in the split view.  If there is no split view, this is nil
    //
    id imageVC = [self.splitViewController.viewControllers lastObject];
    
    // double check to make sure we got an image view controller
    //
    if(![imageVC isKindOfClass:[ImageViewController class]]){
        imageVC = nil;
    }
    
    // return the image view controller or nil
    //
    return imageVC;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    // get the array index associated with the active annotation
    //
    NSInteger arrayIndex = [(MapAnnotation *)view.annotation getDictIndex];
    
    // are we in a split view?
    //
    ImageViewController *splitImageVC = [self splitViewImageViewController];
    
    if(self.locationIsPhoto && splitImageVC){
        
        // we need to display an image in the detail view
        
        splitImageVC.photo = self.locations[arrayIndex];
        [splitImageVC displayPhoto];
        
    } else {
        
        // execute a segue
        
        self.objectSentAcrossSegue = self.locations[arrayIndex];
        
        NSString *segueIdentifier = (self.locationIsPhoto ? @"ImageSegue" : @"PhotoListSegue");
        
        [self performSegueWithIdentifier:segueIdentifier sender:self];
    }
}

@end

//
//  Photo+Lifecycle.h
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 11/18/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import "Photo.h"

@interface Photo (Lifecycle)

- (void)initWithDictionary:(NSDictionary *)dictData inContext:(NSManagedObjectContext *)context;
- (BOOL)deleteInContext:(NSManagedObjectContext *)context;

@end

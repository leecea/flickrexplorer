//
//  Photo+Lifecycle.m
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 11/18/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import "Photo+Lifecycle.h"
#import "FlickrFetcher.h"

@implementation Photo (Lifecycle)

- (void)initWithDictionary:(NSDictionary *)dictData inContext:(NSManagedObjectContext *)context
{
    self.flikrId = [dictData objectForKey:FLICKR_PHOTO_ID];
    self.title = [dictData objectForKey:FLICKR_PHOTO_TITLE];
    self.subtitle = [dictData valueForKeyPath:FLICKR_PHOTO_DESCRIPTION];
    self.saveDate = [[NSDate alloc] init];
    self.dictData = dictData;
    
    //NSString *tags   = [dictData objectForKey:FLICKR_TAGS];
    //self.hasTags = [Tag tagsFromString:tags forPhotoID:self.flikrId inManagedObjectContext:context];
    
    //NSString *place  = [dictData objectForKey:FLICKR_PHOTO_PLACE_NAME];
    //self.wasTakenAt = [Place placeWithName:place inManagedObjectContext:context];
}

- (BOOL)deleteInContext:(NSManagedObjectContext *)context
{
    
    return YES;
}

@end

//
//  Photo.h
//  FlikrPlaces
//
//  Created by Andy Leece on 12/16/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Place, Tag;

@interface Photo : NSManagedObject

@property (nonatomic, retain) id dictData;
@property (nonatomic, retain) NSString * flikrId;
@property (nonatomic, retain) NSString * subtitle;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSDate * saveDate;
@property (nonatomic, retain) NSSet *hasTags;
@property (nonatomic, retain) Place *wasTakenAt;
@end

@interface Photo (CoreDataGeneratedAccessors)

- (void)addHasTagsObject:(Tag *)value;
- (void)removeHasTagsObject:(Tag *)value;
- (void)addHasTags:(NSSet *)values;
- (void)removeHasTags:(NSSet *)values;

@end

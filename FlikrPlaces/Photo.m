//
//  Photo.m
//  FlikrPlaces
//
//  Created by Andy Leece on 12/16/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import "Photo.h"
#import "Place.h"
#import "Tag.h"


@implementation Photo

@dynamic dictData;
@dynamic flikrId;
@dynamic subtitle;
@dynamic title;
@dynamic saveDate;
@dynamic hasTags;
@dynamic wasTakenAt;

@end

//
//  PhotoData.h
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 9/18/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhotoData : NSObject

@property (nonatomic, strong) NSArray *photos;

- (void) initPhotosFromFlickr:(NSDictionary *)place;
- (void) initPhotosFromUserDefaults;
- (NSString *) getTitle:(NSIndexPath *)indexPath;
- (NSString *) getDescription:(NSIndexPath *)indexPath;

@end

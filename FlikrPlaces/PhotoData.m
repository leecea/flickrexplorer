//
//  PhotoData.m
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 9/18/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import "PhotoData.h"
#import "FlickrFetcher.h"
#import "Defaults.h"

@implementation PhotoData

- (void) initPhotosFromFlickr:(NSDictionary *)place
{
    int maxResults = 25;            // add to settings/defaults in future
    
    // get photos for a place from Flickr
    //
    self.photos = [[FlickrFetcher class] photosInPlace:place maxResults:maxResults];
}

- (void) initPhotosFromUserDefaults
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.photos = [defaults arrayForKey:RECENT_PHOTO_KEY];
}

- (NSString *) getTitle:(NSIndexPath *)indexPath
{
    NSString *title = [self.photos[indexPath.row] valueForKey:FLICKR_PHOTO_TITLE];
    
    if([title isEqualToString:EMPTY_STRING]){
        
        // if the title is missing, use the description, and finally the default value "Unknown"
        //
        title = [self getDescription:indexPath];
        if([title isEqualToString:EMPTY_STRING]) title = @"Unknown";
    }
    return title;
}

- (NSString *) getDescription:(NSIndexPath *)indexPath
{
    // Flickr's "description" is itself a dictioanry with an entry for the place name we want to use as our description
    //
    return [self.photos[indexPath.row] valueForKeyPath:FLICKR_PHOTO_DESCRIPTION];
}

@end
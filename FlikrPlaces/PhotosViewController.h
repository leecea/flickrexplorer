//
//  PhotosViewController.h
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 9/9/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotosViewController : UITableViewController

@property (nonatomic, strong) NSDictionary *place;

@end

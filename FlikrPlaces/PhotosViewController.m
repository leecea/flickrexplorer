//
//  PhotosViewController.m
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 9/9/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import "PhotosViewController.h"
#import "PhotoData.h"
#import "ImageViewController.h"
#import "MapViewController.h"
#import "ImageData.h"
#import "PlaceData.h"

@interface PhotosViewController ()

@property (nonatomic, strong) PhotoData *photoData;
@property (nonatomic, strong) UIActivityIndicatorView *spinner;
@property (nonatomic, assign) BOOL viewJustLoaded;

@end

@implementation PhotosViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (PhotoData *) photoData {
    if(_photoData == nil) _photoData = [[PhotoData alloc] init];
    return _photoData;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.viewJustLoaded = YES;
    
    // Set title to first component of place name
    self.navigationItem.title = [PlaceData nameComponents:self.place][0];
}

-(void)viewWillAppear:(BOOL)animated
{
    // The following code should only run when the view first loads BUT an accurate frame size is
    // not available on the iPad until now.  Solution is to use the flag.
    //
    if(self.viewJustLoaded){
        
        self.viewJustLoaded = NO;
        
        self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.spinner.center = self.view.center;
        [self.tableView addSubview:self.spinner];
        
        [self.spinner startAnimating];
        
        dispatch_queue_t downloadQueue = dispatch_queue_create("FlikrPlaces.Background", NULL);
        dispatch_async(downloadQueue, ^{
            
            [self.photoData initPhotosFromFlickr:self.place];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.spinner stopAnimating];
                [self.tableView reloadData];
            });
        });
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.photoData.photos count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PhotoCells";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text = [self.photoData getTitle:indexPath];
    cell.detailTextLabel.text = [self.photoData getDescription:indexPath];
    
    // Ading thumbnail in main thread didn't slow display down
    cell.imageView.image = [ImageData getThumbnail:self.photoData.photos[indexPath.row]];
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"ImageSegue"]){
        
        // get selected row
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        // Set the selected place in the Photo VC
        //
        ImageViewController *imageVC = segue.destinationViewController;
        imageVC.photo = self.photoData.photos[indexPath.row];
    }
    
    if([segue.identifier isEqualToString:@"MapSegue"]){
        
        // Send all the Photos to the Map VC
        //
        MapViewController *mapVC = segue.destinationViewController;
        mapVC.locations = self.photoData.photos;
        mapVC.locationIsPhoto = YES;
    }
}

// check to see whether we are inside a split view controller
//
- (ImageViewController *) splitViewImageViewController
{
    // get last view controller in the split view.  If there is no split view, this is nil
    //
    id imageVC = [self.splitViewController.viewControllers lastObject];
    
    // double check to make sure we got an image view controller
    //
    if(![imageVC isKindOfClass:[ImageViewController class]]){
        imageVC = nil;
    }
    
    // return the image view controller or nil
    //
    return imageVC;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // if we are in a split view, set Photo directly (otherwise let it segue normally)
    //
    ImageViewController *splitImageVC = [self splitViewImageViewController];
    
    if(splitImageVC){
        splitImageVC.photo = self.photoData.photos[indexPath.row];
        [splitImageVC displayPhoto];
    }
}

@end

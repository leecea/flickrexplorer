//
//  PlaceData.h
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 9/10/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlaceData : NSObject

@property (nonatomic, strong) NSArray *places;
@property (nonatomic, strong) NSArray *countryList;
@property (nonatomic, strong) NSArray *countryPlaceList;

- (void) initPlaces;
+ (NSArray *)nameComponents: (NSDictionary *)place;

@end

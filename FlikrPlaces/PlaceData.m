//
//  PlaceData.m
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 9/10/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import "PlaceData.h"
#import "FlickrFetcher.h"

@interface PlaceData ()
@property (nonatomic, strong) NSMutableDictionary *countries;           // private!
@end

@implementation PlaceData

+ (NSArray *)nameComponents:(NSDictionary *)place
{
    NSString *flickrPlaceName = [place valueForKey:FLICKR_PLACE_NAME];
    return [flickrPlaceName componentsSeparatedByString:@","];
}

- (NSMutableDictionary *) countries
{
    if(_countries == nil) _countries = [[NSMutableDictionary alloc] init];
    return _countries;
}

// Get top places from Flickr and build data for section based scrolling list
//
- (void) initPlaces
{
    NSRange range = {0, 75};                                    // limit number of places
    
    self.places = [[FlickrFetcher class] topPlaces];            // get Flickr's top places
    
    if([self.places count] > range.length){
        self.places = [self.places subarrayWithRange:range];    // truncate Flickr's top place list
    }
    
    // sort the places array by country and then by place within country
    //
    self.places = [self.places sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        
        // compare countries
        NSComparisonResult result = [[self getCountry:obj1] compare:[self getCountry:obj2]];      
        
        if(result == NSOrderedSame){
            
            // if countries are the same, compare full place name
            result = [[obj1 valueForKey:FLICKR_PLACE_NAME] compare:[obj2 valueForKey:FLICKR_PLACE_NAME]];
        }
        return result;
    }];

    [self initCountries];                                   // get place lists by country to create sections
}

- (NSString *) getCountry:(NSDictionary *)place
{
    NSString *content = [place valueForKey:FLICKR_PLACE_NAME];
    
    return [[content componentsSeparatedByString:@","] lastObject];  // country is the last value in comma seperated list
}

- (void) initCountries
{
    // loop through the place data returned by Flickr and build the list of countries and an array of place indexes for each country
    //
    NSInteger ix;
    NSUInteger countryIx;
    NSString *country;
    NSArray *placeList;
    
    // working arrays used within the loop below, to build up the data
    //
    NSMutableArray *cList = [[NSMutableArray alloc] init];      // working country list
    NSMutableArray *cpList = [[NSMutableArray alloc] init];     // working country place list
    
    for(ix = 0; ix < [self.places count]; ix++){
        
        country = [self getCountry:self.places[ix]];            // get country name for this place
        countryIx = [cList indexOfObject:country];              // see if that country is in our temp array
        
        if(countryIx == NSNotFound){
            
            // this must be a new country
            //
            [cList addObject:country];
            placeList = [NSArray arrayWithObject:[NSNumber numberWithInteger:ix]];
            [cpList addObject:placeList];
            
        } else {
            
            // add place to array for an existing country
            //
            placeList = [cpList[countryIx] arrayByAddingObject:[NSNumber numberWithInteger:ix]];
            cpList[countryIx] = placeList;
        }
    }
    
    self.countryList = [NSArray arrayWithArray:cList];
    self.countryPlaceList = [NSArray arrayWithArray:cpList];
}
@end

//
//  PlacesAppDelegate.h
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 9/9/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlacesAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

// Added context
//
@property (strong, nonatomic) NSManagedObjectContext *context;

@end

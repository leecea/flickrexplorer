//
//  PlacesViewController.m
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 9/9/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import "PlacesViewController.h"
#import "PhotosViewController.h"
#import "PlaceData.h"   
#import "FlickrFetcher.h"
#import "MapViewController.h"

@interface PlacesViewController ()

@property (nonatomic, strong) PlaceData *placeData;
@property (nonatomic, strong) NSDictionary *selectedPlace;
@property (nonatomic, strong) UIActivityIndicatorView *spinner;

@end

@implementation PlacesViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)queueRefreshRequestAsync {
    
    dispatch_queue_t downloadQueue = dispatch_queue_create("FlikrPlaces.Background", NULL);
    dispatch_async(downloadQueue, ^{
        
        [self.placeData initPlaces];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.spinner stopAnimating];
            [self.tableView reloadData];
        });
    });
    //dispatch_release(downloadQueue);
}

- (IBAction)refreshPlaces:(id)sender {

    [self.spinner startAnimating];
    [self queueRefreshRequestAsync];
}


- (PlaceData *) placeData {
    if(_placeData == nil) _placeData = [[PlaceData alloc] init];
    return _placeData;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.spinner.center = self.view.center;
    [self.tableView addSubview:self.spinner];

    [self.spinner startAnimating];
    [self queueRefreshRequestAsync];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [self.placeData.countryList count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.placeData.countryPlaceList[section] count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *sectionHdr = [[UILabel alloc] init];
    sectionHdr.text = [@" " stringByAppendingString:self.placeData.countryList[section]];
    sectionHdr.textColor = [UIColor whiteColor];
    sectionHdr.backgroundColor = [UIColor colorWithRed:0.58 green:0.67 blue:0.78 alpha:0.8];
    return sectionHdr;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return (CGFloat)25.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PlaceCells";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Get place name and parse it out to eliminate country (since section header is country)
    //
    NSNumber *placeIndex = self.placeData.countryPlaceList[indexPath.section][indexPath.row];
    NSString *flickrPlaceName = [self.placeData.places[[placeIndex integerValue]] valueForKey:FLICKR_PLACE_NAME];
    NSArray *nameComponents = [flickrPlaceName componentsSeparatedByString:@","];
    
    cell.textLabel.text = nameComponents[0];
    cell.detailTextLabel.text = nameComponents[1];
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"PhotoListSegue"]){
        
        // get selected row
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        NSNumber *placeIndex = self.placeData.countryPlaceList[indexPath.section][indexPath.row];
        
        // Set the selected place in the Photo VC
        //
        PhotosViewController *photosVC = segue.destinationViewController;
        photosVC.place = self.placeData.places[[placeIndex integerValue]];
  
        // change back button text
        //
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                       initWithTitle: @"Places"
                                       style: UIBarButtonItemStyleBordered
                                       target: nil action: nil];
        
        [self.navigationItem setBackBarButtonItem: backButton];
    }
    
    if([segue.identifier isEqualToString:@"MapSegue"]){
        
        // Send all the Places to the Map VC
        //
        MapViewController *mapVC = segue.destinationViewController;
        mapVC.locations = self.placeData.places;
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    //PhotosViewController *detailVC = [[PhotosViewController alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
    // Set place that has been selected
    //
    //NSNumber *placeIndex = self.placeData.countryPlaceList[indexPath.section][indexPath.row];
    //self.selectedPlace = self.placeData.places[[placeIndex integerValue]];
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

@end

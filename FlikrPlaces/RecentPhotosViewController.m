//
//  RecentPhotosViewController.m
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 9/24/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import "RecentPhotosViewController.h"
#import "PhotoData.h"
#import "ImageViewController.h"

@interface RecentPhotosViewController ()

@property (nonatomic, strong) PhotoData *photoData;
@property (nonatomic, assign) BOOL didSegue;

@end

@implementation RecentPhotosViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (PhotoData *) photoData {
    if(_photoData == nil) _photoData = [[PhotoData alloc] init];
    return _photoData;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Set title
    self.navigationItem.title = @"Recent Photos";
    
    self.didSegue = NO;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated
{
    // If this view is appearing because we just came back from a segue, there is no need to refresh the recent photo
    // list.  Otherwise we must refresh it.
    //
    if(self.didSegue){
        self.didSegue = NO;     // we just came back from a segue, so reset the flag
    }
    else {
        [self.photoData initPhotosFromUserDefaults];
        [self.tableView reloadData];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];
    self.didSegue = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

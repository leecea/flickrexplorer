//
//  SavedViewController.h
//  FlikrPlaces
//
//  Created by Andy Leece on 1/16/14.
//  Copyright (c) 2014 Leece, Andy W. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataTableViewController.h"
#import "PhotosViewController.h"

@interface SavedViewController : UITableViewController

@end

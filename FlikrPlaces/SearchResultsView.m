//
//  SearchResultsView.m
//  FlikrPlaces
//
//  Created by Andy Leece on 12/20/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import "SearchResultsView.h"

@implementation SearchResultsView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)viewDidLoad
{
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

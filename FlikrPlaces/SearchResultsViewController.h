//
//  SearchResultsViewController.h
//  FlikrPlaces
//
//  Created by Andy Leece on 12/20/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultsViewController : UITableViewController

@property (nonatomic, strong) NSString *searchString;

-(void)displayTable;

@end

//
//  SearchViewController.h
//  FlikrPlaces
//
//  Created by Andy Leece on 12/20/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController <UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end

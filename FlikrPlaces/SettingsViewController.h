//
//  SettingsViewController.h
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 9/25/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UITableViewController

@end

//
//  Tag.h
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 11/13/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Photo;

@interface Tag : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *relatedPhotos;
@end

@interface Tag (CoreDataGeneratedAccessors)

- (void)addRelatedPhotosObject:(Photo *)value;
- (void)removeRelatedPhotosObject:(Photo *)value;
- (void)addRelatedPhotos:(NSSet *)values;
- (void)removeRelatedPhotos:(NSSet *)values;

@end

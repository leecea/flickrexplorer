//
//  Tag.m
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 11/13/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import "Tag.h"
#import "Photo.h"


@implementation Tag

@dynamic name;
@dynamic relatedPhotos;

@end

//
//  main.m
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 9/9/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PlacesAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PlacesAppDelegate class]));
    }
}

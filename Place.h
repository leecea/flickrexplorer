//
//  Place.h
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 11/13/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Photo;

@interface Place : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *hasPhotos;
@end

@interface Place (CoreDataGeneratedAccessors)

- (void)addHasPhotosObject:(Photo *)value;
- (void)removeHasPhotosObject:(Photo *)value;
- (void)addHasPhotos:(NSSet *)values;
- (void)removeHasPhotos:(NSSet *)values;

@end

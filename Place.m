//
//  Place.m
//  FlikrPlaces
//
//  Created by Leece, Andy W. on 11/13/13.
//  Copyright (c) 2013 Leece, Andy W. All rights reserved.
//

#import "Place.h"
#import "Photo.h"


@implementation Place

@dynamic name;
@dynamic hasPhotos;

@end
